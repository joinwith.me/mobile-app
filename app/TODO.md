#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!READ THIS ZIGMAS!!!!!!!!!!!!!!!!!!!!!!!!

FIX: Load events every single time! IMPORTANT

fix viewing event details page.
fix creating an event page to return back to the map and update the markers of the map with computed properties
add profile settings so user could update his data.


DESIGN:

User signs in with  facebook, app  created a copy of the firebase  auth credentials and stores it in the store
saving it together to the 

[ ] Have a button that  when clicked it would focus on user location, point to where user is.
[ ] Have a better looking design
[ ] Have a user profile page view
[ ] Have a chat !!!

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!END OF READ THIS ZIGMAS!!!!!!!!!!!!!!!!!!!!!!!!

The TODO List for the application

! IMPORTANT !

Check why the Android emulator is using 40% of my CPU on macbook.


# Single source of truth


* [√] Multiple default events on different positions
* [√] One of which the default user is the owner.
* [√] One of which the default user is a participant.
* [√] Have a default user fot the application (John Doe)
* [] Define data structures and modules for store
* [] Create store for users
* [] Create store for events
* [√] Only retrieve all active events from firestore that resolved with `deleted` property as false.


    
**[√] Create an event screen**

    [] Synchronize changes from global eventbus 'committed-events' to Create.vue component
    [] update store

[] Join events
    [√] Update that events participants data (list)
    [√] Update events participants list on firestore 
    [√] User cannot join his own events - should show him > manage or cancel event.
    [~] Owner of event should be able to cancel the event.

[x] Load initial events information from firestore:
    [√] 1. loads data from firestore
    [√] 2. commits it to the state checking if theres no such data already.
    [√] 3. emits a global event that should be caught by the map component
    [v] 4. map component should listen to those events and update the missing pointers
    [] 5. Create a listener for live data, once data is updated/added within firestore, it should be updated in the state of store as well.

# Events

## Store

### Actions 
* [√] Create event
* [√] Join event
* [√] Leave event
* [] Delete event

## Data
* [√] Get events list (data) that you have created.
* [√] Get events list (data) that you've joined.
* [√] Return the collection (data) of them combined.
* [] Create a view where people would be able to edit them if they're the owner of the event.

### Manage created events
 * [] Cancel created event:

    Set that events deleted property to `true` and deletedAt to timestame, update the firestore document with changed data.

    
### Manage joined events
* [] Create a 'Not going' button next to the event that you're participating at to leave the event.


[] Manage your profile.
    * What should a user be able to manage?
[] Create routes separately
    [] Login view
    [] Create event view
    [] Main map view
    [] Detail event view
    [] Manage event view
    [] Your events view
[] Setup Firebase for iOS and Android.
    [] Create a Facebook application for signing in with facebook.
    [] (optional) Create a Google sign in?
    [] Learn Firebase Firestore
    [] Login with Facebook and store data in Firebase database.
    [] Check again which databse of Firebase do I need.


# Random tasks:

[] Check for terminal colors in vuex logger