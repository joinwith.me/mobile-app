
export const mockUser = {
  name: 'John', surname: 'Doe',
  id: "some_user_key_jhn13f3jgx",
  date_joined: "2018-10-09 10:09:12",
  last_active: "2018-10-10 10:10:39" ,
  // list names of events that the user created himself:
  // own_events, created_events, events_created, self_created, userCreatedEvents, user_created_events 
  eventsCreated: ['event_no_14af23',],
  // list names of events that the user has joined:
  // events_joined, userJoinedEvents, user_joined_events, 
  eventsJoined: ['event_no_4agnkag',],
  radius: 500, // radius will be defined in meters.
  pircture: 'someurl', //url to a picture or a resource.
  interests: ['basketball', 'programming', 'technology', 'meetups']
}

export const mockEventData = {
  event_no_z294fn4: {
    id: 'event_no_z294fn4',
    lat: 37.74673470005098, lng: -121.41955118849648,
    name: 'Basketball game',
    description: 'Basketball is a fun sports to play, join me!',
    owner: "some_random_user_key_gas2322",
    date: '2018-10-10',
    time: '10:10',
    participants: [],
    deleted: false,
    createdAt: new Date().getTime(),
    deletedAt: null,
    modifiedAt:null,
  },
  event_no_m4tgag: {
    id: 'event_no_m4tgag',
    lat: 37.72549097200445, lng: -121.43584080239873,
    name: 'Pool game',
    description: 'Lets knock some balls down the table.',
    owner: "some_random_user_key_flam2t2gam",
    date: '2018-10-11',
    time: '11:11',
    participants: [],
    deleted: false,
    createdAt: new Date().getTime(),
    deletedAt: null,
    modifiedAt:null,
  },
  event_no_4agnkag: {
    id: 'event_no_4agnkag',
    lat: 37.760448015748025, lng: -121.40747213352222,
    name: 'Theather show',
    description: 'Lets meet up and watch a show together!',
    owner: "some_random_user_key_fresco23rt34",
    date: '2018-10-12',
    time: '12:12',
    participants: [mockUser.id],
    deleted: false,
    createdAt: new Date().getTime(),
    deletedAt: null,
    modifiedAt:null,
  },
  event_no_14af23: {
    id: 'event_no_14af23',
    lat: 37.732864319116544, lng: -121.41026977179331,
    name: 'Group programming sessin',
    description: 'Lets pair program and build something cool. Hackathon style!',
    owner: mockUser.id,
    date: '2018-10-13',
    time: '13:13',
    participants: [],
    deleted: false,
    createdAt: new Date().getTime(),
    deletedAt: null,
    modifiedAt:null,
  }
}