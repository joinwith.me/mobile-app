import { firestore } from "nativescript-plugin-firebase";

let eventDocRef = null

export default class EventService {
  

  get eventDoc(id) {
    return firebase.firestore.collection('events').doc(id)
  }

  joinEvent(eventId, userId) {
    eventDocRef.update({
      participants: firebase.firestore.FieldValue.arrayUnion(this.user.id)
    });
  }
}
