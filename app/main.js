// import './tslib.nativescript';
import { isEmpty } from 'lodash'
import Vue from 'nativescript-vue'
import App from './components/App'
import Login from './components/Login'
import { TNSFontIcon, fonticon } from 'nativescript-fonticon';
import store from './store'
import firebase from 'nativescript-plugin-firebase'
// import VueDevtools from 'nativescript-vue-devtools'
import * as appSettings from "tns-core-modules/application-settings";
import { INIT_USER_DATA } from '@/store/types'
import VueDevtools from 'nativescript-vue-devtools'
Vue.use(VueDevtools)
// Vue.prototype.$mapboxAccessToken = "pk.eyJ1IjoienNsdXNueXMiLCJhIjoiY2l3bWk2MWxnMDAwajJ2cDdraGxoZGFiMCJ9.T_pCtda9Twn-oCsNF7Nrjw"

// initialize firebase 

TNSFontIcon.debug = true;
TNSFontIcon.paths = {
    'fa': './fonts/fa5-all.css'
};
TNSFontIcon.loadCss();
Vue.filter('fonticon', fonticon);

// Additional dependencies
// TODO: Add offline maps support for when user doesn't have internet, 
// but his events would still work and the map would be showed at the last point where he was looking before
// if(TNS_ENV !== 'production') {
// Vue.use(VueDevtools, {host: '192.168.0.109', port: 8098})
// // Vue.use(VueDevtools)

// }
// // Prints Vue logs when --env.production is *NOT* set while building
Vue.config.silent = (TNS_ENV === 'production')

//check if internet connection is on.


// store.dispatch('loadUser')

//TODO: When application has internet connection, allow firebase connecting.
// application.hasInternet() {
//   // If application has internet connection, load firebase map data
//   // For now im disabling the events loading from firestore.
//   store.dispatch('loadEvents')
// }


  ;(async () => {

    Vue.registerElement('RadSideDrawer', () => require('nativescript-ui-sidedrawer').RadSideDrawer)
    Vue.registerElement("Mapbox", () => require("nativescript-mapbox").MapboxView)
    
    store.dispatch(INIT_USER_DATA)
    let isLoggedIn = store.getters.isAuthenticated
    console.log('isLoggedIn :', isLoggedIn);
    let renderStartComponent = Login
    
    try {
      firebase.init(
        {
          onAuthStateChanged: async (data) => { // optional but useful to immediately re-logon the user when he re-visits your app
            console.log(data.loggedIn ? "Logged in to firebase" : "Logged out from firebase");
            // console.log('data of auth state object :', data);
            if (data.loggedIn) {
              isLoggedIn = true
              console.log("user's email address: " + (data.user.email ? data.user.email : "N/A"));
              appSettings.setString('user', JSON.stringify(data['user']))
              // appSettings.remove('user')
              // store.dispatch('CHECK_LOGGED_IN')
            }
          }
        })
      } catch (error) {
        console.log(error)
      }
      
  
      if (isLoggedIn)  {
        await store.dispatch('CHECK_LOGGED_IN')
        renderStartComponent = App
      }
    
      new Vue({
        store, render: h => h('frame', [h(renderStartComponent)])
      }).$start()
  })()

