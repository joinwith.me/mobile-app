import Create from '@/components/Create'
import EditProfile from '@/components/EditProfile'

export {
  Create,
  EditProfile
}