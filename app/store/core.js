import Vue from 'vue'
import * as types from './types'
import { firestore } from 'nativescript-plugin-firebase';
import EventBus from '~/event-bus'
import firebase from 'nativescript-plugin-firebase'
import * as appSettings from "tns-core-modules/application-settings";
import { get, isEmpty } from 'lodash'
// import { INIT_USER_DATA } from './types'

const AUTH_TOKEN_KEY = 'FB_TOKEN' 

const state = {
  user: null,
  globalSettings: {
    settings: null
  },
  stateMessage: null,
  errors: null,
  events: {},
  loading: false,
}

const gettersObject = {
  isLoading: state => state.loading,
  getUser: state => state.user,
  isAuthenticated: state => !!state.user,
  company: state => state.company,
  stateMessage: state => state.stateMessage,
  errors: state => state.errors,
  getEvents: state => {
    console.log('events getter triggered');
    return Object.keys(state.events).map(id => state.events[id])
  },
  getEventById: state => id => state.events[id],
  getUserEventsCreated: state => Object.keys(state.events).map(id => state.events[id]).filter(itm => itm.owner === state.user.uid),
  getEventsUserJoined: state => Object.keys(state.events).map(id => state.events[id]).filter(itm => itm.participants.includes(state.user.uid)),
  getUserEventsJoined: state => Object.keys(state.events).map(id => state.events[id]).filter(itm => itm.participants.includes(state.user.uid)),
}

const APP_SETTING_USER_KEY = 'user'

const actions = {
  async loadEvents({ state, commit }) {
    // Empty object for loaded events with ids
    console.log('YOOO ITS LOADING EVENTS!');
    let loadedEventsMap = {}
    let loadedEventsRef = await firestore.collection('events').where("deleted", "==", false).get()
    console.log('loadedEventsRef :', loadedEventsRef);
    const existingEventIds = Object.keys(state.events)
    // Put all loaded events to the object
    loadedEventsRef.forEach(doc => {
      loadedEventsMap[doc.id] = doc.data()
    })
    // Delete evenets which are already in the store, put into a list those which are not
    const notInStoreEventsList = Object.keys(loadedEventsMap).map(id => {
      if (existingEventIds.includes(id)){
        delete loadedEventsMap[id]
      } else {
        return loadedEventsMap[id]
      }
    })
    // Commit the object of events to the state
    commit('addEvents', loadedEventsMap)
    // console.log('[action]: loadEvents - BEFORE EMITTING committed-events')
    // Pass the list of events to the map so markers are created.
    // EventBus.$emit('committed-events', notInStoreEventsList)
    // console.log('[action]: loadEvents - AFTER EMITTING committed-events')
  },

  async createEvent({getters, state, commit }, payload) {
    commit('SET_LOADING', true)
    const newEvent = payload
    const isEventInStore = !!state.events[newEvent.id]
    const user = {...state.user}
    
    if (!isEmpty(isEventInStore)) {
      console.log('event is in the store already')
      return false
    }
    
    if (isEmpty(user)) throw new Error('[CRITICAL] User has not been detected while creating an event!')

    try {
      // Check if there is no event and if there is, stop the process
      
      const userRef = await firestore.collection('users').doc(user.uid)
      await userRef.update({
        eventsCreated: firestore.FieldValue.arrayUnion([user.uid])
      });

      // Create event
      const eventRef = await firestore.collection('events').doc()
      newEvent['id'] = eventRef.id
      await eventRef.set(newEvent)

      // Update user instance
      // user in store should be updated as well
      
      let userCopy = Object.assign({}, user)
      userCopy.eventsCreated.push(eventRef.id)
      
      commit('updateUser', userCopy)
      commit('addNewEvent', newEvent)
      // EventBus.$emit('committed-events', [...[newEvent]])
      commit('SET_LOADING', false)

    } catch (error) {
      console.log(error)
      commit('SET_LOADING', false)

      throw error
    }

  },
  async joinEvent({ commit, getters }, payload) {
    // Extract required information from the payload
    const {eventId, userId} = payload
    const event = Object.assign({}, getters.getEventById(eventId) || {})

    // permit actions if event doesnt exist 
    if (Object.keys(event).length === 0) {
      alert('Theres no event to join, it may have been deleted.')
      return
    }
    // Update the firestore databse
    try {
      const eventDocRef = await firestore.collection('events').doc(eventId)
      await eventDocRef.update({
        participants: firestore.FieldValue.arrayUnion([userId])
      });
    } catch (error) {
      console.log('[ ERROR ] Could not find an event you\'re trying to join.')
      throw error
    }
    // Update the event in store with new participants list.
    const updatedParticipants = [...event['participants'], userId]
    event['participants'] = updatedParticipants
    commit('addNewEvent', event)
  },

  async leaveEvent({ commit, getters }, payload) {
    const {eventId, userId} = payload
    const event = Object.assign({}, getters.getEventById(eventId) || {})
    // Check if theres such event object
    if (isEmpty(event)) {
      console.log('[WARNING]: Theres no such event object')
      return false
    }
    try {
      const eventDocRef = await firestore.collection('events').doc(eventId)
      await eventDocRef.update({
        participants: firestore.FieldValue.arrayRemove([userId])
      });
    } catch (error) {
      console.log(error)
      throw error
    }
    // Update the store
    const participants = event.participants.filter(participant => participant !== userId)
    event.participants = participants
    commit('leaveEvent', event)
  },
  async cancelEvent({ commit, getters }, { eventId }) {
    const event = get(state.events, eventId, {}) 
    const deletedAt = firestore.FieldValue.serverTimestamp()
    console.log('deletedAt - from firestore :', deletedAt);
    if (isEmpty(event)) {
      console.log('[WARNING]: Theres no such event object')
      return false
    }
    
    if (!event.deleted) 
    {
      const payload = {
        deleted: true,
        deletedAt,
      }
      // Remove event from firebase, mark it as deleted.
      try {
        await firestore.collection('events').doc(event.id).update(payload)
      } catch (error) {
        console.log(error)
        throw error
      }
      console.log('SUCCESSFULLY DELETED')
      // Assigning event values
      Object.assign(event, payload)
      commit('deleteEvent', event)
      console.log('BEFORE DELETING MARKERS')
      // EventBus.$emit('deleteEventMarkers', [eventId])

    } else {
      console.log('THIS EVENT IS ALREADY DELETED')
    }
  },

  async updateProfile({ commit }, data) {
    const { name, surname, interests } = data

    if (name === '' || surname === '' || interests === '') return
    data.interests = data.interests.split(' ')
    const payload = {...data}
    try {
      commit('setUserData' , payload)
    } catch (error) {
      return error
    }
  },

  // INITIALIZE USER DATA FROM APP SETTINGS
  [types.INIT_USER_DATA]({ state, commit }) {
    const userAppSetting = appSettings.getString(APP_SETTING_USER_KEY)
    const loadedUser = userAppSetting ? JSON.parse(userAppSetting) : null

    const tempUser = { uid: 'RBmA3yCeQAN3xnmtkiljlpzdAvz1',
    name: 'Zigmas',
    email: 'bkzlamhiit_1547500021@tfbnw.net',
    emailVerified: false,
    providers: [ ],
    anonymous: false,
    isAnonymous: false,
    phoneNumber: null,
    profileImageURL: 'https://graph.facebook.com/100850674357369/picture',
    metadata:
    { creationTimestamp: 'Tue Jan 15 2019 11:34:27 GMT+0200 (EET)',
    lastSignInTimestamp: 'Mon Feb 04 2019 20:33:10 GMT+0200 (EET)' },
    additionalUserInfo:
    { providerId: 'facebook.com',
    username: null,
    isNewUser: false,
    profile: [Object] },
    eventsCreated: [] } 

    if (isEmpty(state.user) && loadedUser) {
      console.log('should load: ', loadedUser)
      commit(types.SET_USER, loadedUser)
    }
  },

  async ['LOGIN_FACEBOOK']({ commit, dispatch }) {
    try {
      const result = await firebase.login({
        type: firebase.LoginType.FACEBOOK,
        // Optional
        facebookOptions: {
          // defaults to ['public_profile', 'email']
          scope: ['public_profile', 'email']
        }
      })

      const { providers } = result
      const facebookProvider = providers.find( provider => provider.id === 'facebook.com' )
      appSettings.setString(AUTH_TOKEN_KEY, facebookProvider.token)
      // SETU USER INTO APP SETTINGS
      appSettings.setString(APP_SETTING_USER_KEY, JSON.stringify(result))
      await dispatch('CHECK_LOGGED_IN')
      // update user reference in firebase database
      // let userRef = firestore.collection('users').doc(user.uid)
      // userRef.update(user)
    } catch (error) {
      console.log('error trying to login: ', error)
    }
  },

  async ['CHECK_LOGGED_IN']({ commit }) {
    // TODO: check await signals. when using await it breaks the app.
    let user = await firebase.getCurrentUser()

    if (!isEmpty(user)) {
      let userRef = await firestore.collection('users').doc(user.uid)
      userRef.set(user, { merge: true })

    }
    const userObject = Object.freeze({...user, eventsCreated: []})
    commit(types.SET_USER, userObject)

  },

  logout({ commit }) {
    firebase.logout()
    appSettings.remove(APP_SETTING_USER_KEY)
    commit(types.SET_USER, null)
  }
}

const mutations = {
  setUserData: (state, payload) => {
    state.user = {...payload}
  },
  ['SET_LOADING'](state, isLoading) {
    state.loading = isLoading
  },

  [types.SET_USER](state, payload) {
    state.user = payload
  },
  addEvents: (state, payload) => {
    const newEvents = { ...state.events, ...payload }
    state.events = newEvents
  },
  // Adds new or updates existing event in the store
  addNewEvent: (state, payload) => {
    Vue.set(state.events, payload.id, payload)
  },
  leaveEvent: (state, eventPayload) => {
    Vue.set(state.events, eventPayload.id, eventPayload)
  },
  deleteEvent: (state, payload) => {
    const {id, deleted, deletedAt } = payload 
    Vue.set(state.events[id], 'deleted', deleted)
    Vue.set(state.events[id], 'deletedAt', deletedAt)
  },

  logout: (state) => {
    state.user = null
  },

  ['SET_ERROR'](state, errors) {
    state.errors = errors
  }
}

export default {
  namespaced: false,
  state,
  getters: gettersObject,
  actions,
  mutations
}
