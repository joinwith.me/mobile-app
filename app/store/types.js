export const LOAD_USER = 'LOAD_USER';
export const SET_USER = 'SET_USER';

export const USER_JOIN_EVENT = 'USER_JOIN_EVENT';

export const INIT_USER_DATA = 'INIT_USER_DATA'

export const ACTION_LOADING = 'ACTION_LOADING'