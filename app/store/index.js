import Vue from 'vue';
import Vuex from 'vuex';
import createLogger from 'vuex/dist/logger';

import core from './core'

const debug = process.env.NODE_ENV !== 'production';

Vue.config.devtools = true;
Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    someItems: [],
  }, 
  modules: {
    core
  },
  strict: debug,
  plugins: debug ? [createLogger()] : [],
});
